<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddAdditionalTableToUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->string('nickname');
            $table->string('tgl-lahir');
            $table->string('bulan-lahir');
            $table->string('tahun-lahir');
            $table->string('status-pernikahan');
            $table->string('gender');
            $table->string('social-fb');
            $table->string('social-ig');
            $table->string('social-tw');
            $table->string('ukuran-jacket');
            $table->string('attendance');
            $table->string('bantuan');
            $table->longText('message');
            $table->string('subject');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('user', function (Blueprint $table) {
            Schema::drop('user');
        });
    }
}
