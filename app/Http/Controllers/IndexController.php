<?php

namespace App\Http\Controllers;

use App\UserDB;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{
    public function index()
    {
        // $data = UserDB::all();
        $data = [];

        return view('index', compact('data'));
        // return view('index');
    }

    public function getDouble()
    {
        // $data = UserDB::all();
        $data = [];

        return view('check_duplicate', compact('data'));
        // return view('index');
    }

    public function delete(Request $request)
    {
        $delete = UserDB::destroy($request->data);

        return 'deleted';
        
    }

    public function getAll()
    {
        $data = UserDB::all();

        return datatables($data)->setRowData([
            'data-id' => function ($data)
            {
                return "row-".$data->id;
            }
        ])->toJson();
    }

    public function importCSV(Request $request)
    {
        // dd($request);
        $upload = $request->file('csv_file');
        $filePath = $upload->getRealPath();

        ini_set('auto_detect_line_endings',TRUE);

        $file = fopen($filePath, 'r');

        $header = fgetcsv($file);
        // dd($header);

        $escapedheader = [];

        foreach ($header as $key => $value) {
            // dd($value);
            $lheader = strtolower($value);
            $escapedItem = preg_replace('/[^a-z]/','',$lheader);
            array_push($escapedheader, $escapedItem);
        }
        
        while ($columns = fgetcsv($file)) {
            if ($columns[0]=="") {
                continue;
            }

            foreach ($columns as $key => $value) {
                $value = preg_replace('/\D/','',$value);
            }

            $data = array_combine($escapedheader, $columns);

            // dd($escapedheader);
            $name = $data['yourname'];
            $email = $data['youremail'];
            $angkatan = $data['angkatan'];
            $address = $data['address'];
            $phone = "0".$data['nohp'];
            $blood = $data['goldarah'];
            $nick = $data['yournickname'];
            $tgl = $data['tanggallahir'];
            $bln = $data['bulanlahir'];
            $thn = $data['tahunlahir'];
            $status = $data['statuspernikahan'];
            $gender = $data['jeniskelamin'];
            $fb = $data['socialig'];
            $ig = $data['socialfb'];
            $tw = $data['socialtw'];
            $jacket = $data['ukuranjaket'];
            $attendance = $data['attendance'];
            $help = $data['bantuan'];
            $message = $data['yourmessage'];
            $subject = $data['yoursubject'];

            // dd($data);
            $insertData = UserDB::create([
                'name'=>$name,
                'email'=>$email,
                'angkatan'=>$angkatan,
                'address'=>$address,
                'phone'=>$phone,
                'gol-darah'=>$blood,
                'nickname'=>$nick,
                'tgl-lahir'=>$tgl,
                'bulan-lahir'=>$bln,
                'tahun-lahir'=>$thn,
                'status-pernikahan'=>$status,
                'gender'=>$gender,
                'social-fb'=>$fb,
                'social-ig'=>$ig,
                'social-tw'=>$tw,
                'ukuran-jacket'=>$jacket,
                'attendance'=>$attendance,
                'bantuan'=>$help,
                'message'=>$message,
                'subject'=>$subject,
            ]);            

            
        }
        return redirect()->route('home');   
    }

    public function checkDuplicate()
    {
        $data = UserDB::whereIn('name', function ($query)
        {
            $query->select('name')->from('user')->groupBy('name')->havingRaw('count(*) > 1');
        })->get();
        
        // $data = DB::table('user')
        //     ->select('name')
        //     ->groupBy('name')
        //     ->havingRaw('count(*) > 1')
        //     ->get();

        return datatables($data)->setRowData([
            'data-id' => function ($data)
            {
                return "row-".$data->id;
            }
        ])->toJson();
    }
}
