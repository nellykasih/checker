<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserDB extends Model
{
    protected $table = "user";

    protected $fillable = [
        'name',
        'email',
        'gender',
        'angkatan',
        'address',
        'phone',
        'gol-darah',
        'nickname',
        'tgl-lahir',
        'bulan-lahir',
        'tahun-lahir',
        'status-pernikahan',
        'gender',
        'social-fb',
        'social-ig',
        'social-tw',
        'ukuran-jacket',
        'attendance',
        'bantuan',
        'message',
        'subject',
    ];
}
