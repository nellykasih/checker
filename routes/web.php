<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'IndexController@index')->name('home');
Route::post('/delete/{data}', 'IndexController@delete');

// Import CSV
Route::post('/import', 'IndexController@importCSV')->name('import');

// Fetch data
Route::get('/data/getAll', 'IndexController@getAll');

// Check Duplicate
Route::get('/double', 'IndexController@getDouble')->name('table.duplicate');
Route::get('/data/duplicate', 'IndexController@checkDuplicate');
