jQuery(document).ready(function($) {
    'use strict';
    var selectedData = []

    fetch_data();

    $('#delete-data').prop('disabled', true)

    $(document).on('click', $('table.first').DataTable().rows(), function () {
        if (!$('table.first').DataTable().rows('.selected').any()) {
            $('#delete-data').prop('disabled', true)
        } else {
            $('#delete-data').prop('disabled', false)
        }
    })

    $(document).on('click', $('table.duplicate').DataTable().rows(), function () {
        if (!$('table.duplicate').DataTable().rows('.selected').any()) {
            $('#delete-data').prop('disabled', true)
        } else {
            $('#delete-data').prop('disabled', false)
        }
    })

    $(document).on('click', '#delete-data', function(){
      delete_data();
    })

    $(document).on('click', '#delete-duplicate-data', function(){
      delete_duplicate_data();
    })

    // $('#btn-upload').on('click', function () {
    //     event.preventDefault();
        
    //     var uploadFile = new FormData();
    //     var csv = $('#csv_file')
    //     var csv_file = csv[0].files[0]
    //     uploadFile.append('file',csv_file)

    //     console.log(uploadFile)
    //     $.ajax({
    //         url: 'import',
    //         method: 'POST',
    //         data:{"_token": $('meta[name="csrf-token"]').attr('content'), "file": uploadFile},
    //         contentType: false,
    //         cache: false,
    //         processData: false,
    //         // dataType: 'json',
    //         success: function (data){

    //             alert(data);
    //             table.destroy()
    //             fetch_data()
    //         }
    //     })
    // })
    /*
    if ($("table.first").length) {

        $(document).ready(function() {
            $('table.first').DataTable({
                // dom: '<"top"B>lfrtip',
                dom: 
                    "<'row'<'col-sm-12 pb-3'B>>" +
                    "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
                    "<'row'<'col-sm-12'tr>>" +
                    "<'row'<'col-sm-5'i><'col-sm-7'p>>",
                // ajax: "",
                buttons: [
                    // { extend: 'remove', editor: editor },
                    // 'copy', 'excel', 'pdf',s
                    {
                        extend: 'csv',
                        text: 'Export CSV',
                    },
                    {
                        text: 'Import CSV',
                        action: function () {
                            uploadEditor.create( {
                                title: 'CSV file import'
                            } );
                        }
                    },
                ],
                select: {
                    style: 'multi'
                },
            });



            $('.dt-buttons').append(
              "<button id='delete-data' class='btn btn-outline-light' tabindex='0' aria-controls='DataTables_Table_0' type='button' > "+
              "<span>Delete</span>" +
              "</button>"
            )
        });

        $(document).on('click', '#delete-data', function(){
          var id = $('.odd.selected').data('id');
          var origin = window.location.origin;

          if (confirm('Hapus data ini ?')) {
            $.ajax({
            //   url:origin+"/"+id,
              url:origin+"/data/getAll",
              method:'GET',
              success:function (data) {
                // $('table.first').dataTable().destroy();
                
                fetch_data();
              }
            })
            
          }
        })
    }
    **/

    /* Calender jQuery **/

    // if ($("table.second").length) {

    //     $(document).ready(function() {
    //         var table = $('table.second').DataTable({
    //             lengthChange: false,
    //             buttons: ['copy', 'excel', 'pdf', 'print', 'colvis']
    //         });

    //         table.buttons().container()
    //             .appendTo('#example_wrapper .col-md-6:eq(0)');
    //     });
    // }


    if ($("#example2").length) {

        $(document).ready(function() {
            $(document).ready(function() {
                var groupColumn = 2;
                var table = $('#example2').DataTable({
                    "columnDefs": [
                        { "visible": false, "targets": groupColumn }
                    ],
                    "order": [
                        [groupColumn, 'asc']
                    ],
                    "displayLength": 25,
                    "drawCallback": function(settings) {
                        var api = this.api();
                        var rows = api.rows({ page: 'current' }).nodes();
                        var last = null;

                        api.column(groupColumn, { page: 'current' }).data().each(function(group, i) {
                            if (last !== group) {
                                $(rows).eq(i).before(
                                    '<tr class="group"><td colspan="5">' + group + '</td></tr>'
                                );

                                last = group;
                            }
                        });
                    }
                });

                // Order by the grouping
                $('#example2 tbody').on('click', 'tr.group', function() {
                    var currentOrder = table.order()[0];
                    if (currentOrder[0] === groupColumn && currentOrder[1] === 'asc') {
                        table.order([groupColumn, 'desc']).draw();
                    } else {
                        table.order([groupColumn, 'asc']).draw();
                    }
                });
            });
        });
    }

    if ($("#example3").length) {

        $('#example3').DataTable({
            select: {
                style: 'multi'
            }
        });

    }
    if ($("#example4").length) {

        $(document).ready(function() {
            var table = $('#example4').DataTable({
                fixedHeader: true
            });
        });
    }

    /**
     * Function
     */
    function fetch_data() {
        var groupColumn = 3;

        var fetch = $('table.first').DataTable({
            "columnDefs": [
                { "visible": false, "targets": groupColumn,}
            ],
            "order": [
                [groupColumn, 'asc']
            ],
            dom: 
            "<'row'<'col-sm-12 pb-3'B>>" +
            "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [
                {
                    extend: 'csv',
                    text: 'Export CSV'
                },
                {
                    text: 'Import CSV',
                    action: function () {
                        $('#exampleModalCenter').modal('toggle')
                    }
                },
                'selectAll', 'selectNone', 'colvis',
            ],
            processing : true,
            serverSide : true,
            ajax  : 'data/getAll',
            columns : [
                { data:'name', name:'name' },
                { data:'nickname', name:'nickname' },
                { data:'email', name:'email' },
                { data:'angkatan', name:'angkatan' },
                { data:'address', name:'address' },
                { data:'phone', name:'phone' },
                { data:'gol-darah', name:'goldarah' },
                { data:'tgl-lahir', name:'tanggal-lahir' },
                { data:'bulan-lahir', name:'bulan-lahir' },
                { data:'tahun-lahir', name:'tahun-lahir' },
                { data:'status-pernikahan', name:'status' },
                { data:'gender', name:'gender' },
                { data:'social-fb', name:'fb' },
                { data:'social-ig', name:'ig' },
                { data:'social-tw', name:'tw' },
                { data:'ukuran-jacket', name:'jacket' },
                { data:'attendance', name:'attendance' },
                { data:'bantuan', name:'bantuan' },
                { data:'message', name:'message' },
                { data:'subject', name:'subject' },
            ],
            select: {
                style: 'multi'
            },
            "drawCallback": function(settings) {
                var api = this.api();
                var rows = api.rows({ page: 'current' }).nodes();
                var last = null;

                api.column(groupColumn, { page: 'current' }).data().each(function(group, i) {
                    if (last !== group) {
                        $(rows).eq(i).before(
                            '<tr class="group"><td colspan="100">' + group + '</td></tr>'
                        );

                        last = group;
                    }
                });
            },
            lengthMenu: [[25, 100, -1], [25, 100, "All"]],
            pageLength: 25,
            search: {
               regex: true,
               smart: true
            }
        })


        var fetch2 = $('table.duplicate').DataTable({
            // "columnDefs": [
            //     { "visible": false, "targets": groupColumn,}
            // ],
            // "order": [
            //     [groupColumn, 'asc']
            // ],
            dom: 
            "<'row'<'col-sm-12 pb-3'B>>" +
            "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
            "<'row'<'col-sm-12'tr>>" +
            "<'row'<'col-sm-5'i><'col-sm-7'p>>",
            buttons: [
                {
                    extend: 'csv',
                    text: 'Export CSV'
                },
                // {
                //     text: 'Import CSV',
                //     action: function () {
                //         $('#exampleModalCenter').modal('toggle')
                //     }
                // },
                'selectAll', 'selectNone', 'colvis',
            ],
            processing : true,
            serverSide : true,
            ajax  : 'data/duplicate',
            columns : [
                { data:'name', name:'name' },
                { data:'nickname', name:'nickname' },
                { data:'email', name:'email' },
                { data:'angkatan', name:'angkatan' },
                { data:'address', name:'address' },
                { data:'phone', name:'phone' },
                { data:'gol-darah', name:'goldarah' },
                { data:'tgl-lahir', name:'tanggal-lahir' },
                { data:'bulan-lahir', name:'bulan-lahir' },
                { data:'tahun-lahir', name:'tahun-lahir' },
                { data:'status-pernikahan', name:'status' },
                { data:'gender', name:'gender' },
                { data:'social-fb', name:'fb' },
                { data:'social-ig', name:'ig' },
                { data:'social-tw', name:'tw' },
                { data:'ukuran-jacket', name:'jacket' },
                { data:'attendance', name:'attendance' },
                { data:'bantuan', name:'bantuan' },
                { data:'message', name:'message' },
                { data:'subject', name:'subject' },
            ],
            select: {
                style: 'multi'
            },
            // "drawCallback": function(settings) {
            //     var api = this.api();
            //     var rows = api.rows({ page: 'current' }).nodes();
            //     var last = null;

            //     api.column(groupColumn, { page: 'current' }).data().each(function(group, i) {
            //         if (last !== group) {
            //             $(rows).eq(i).before(
            //                 '<tr class="group"><td colspan="100">' + group + '</td></tr>'
            //             );

            //             last = group;
            //         }
            //     });
            // },
            lengthMenu: [[10, 25, 100, -1], [10, 25, 100, "All"]],
            pageLength: 25,
            search: {
               regex: true,
               smart: true
            }
        })

        $('.dt-buttons').append(
          "<button id='delete-data' class='btn btn-outline-light' tabindex='0' aria-controls='DataTables_Table_0' type='button' > "+
          "<span>Delete</span>" +
          "</button>"
        )
    }

    function delete_data() {
        var count = [];
        var origin = window.location.origin;
        var table = $('table.first').DataTable()
        if ($('table').hasClass('duplicate')) {
            table = $('table.duplicate').DataTable()
        }
        console.log(table)

        selectedData = table.rows('.selected').data()

        for (let index = 0; index < selectedData.length; index++) {
            count.push(selectedData[index]['id'])
        }
        
        if(confirm("Are you sure you want to remove this?"))
        {
            // count.serialize()
            console.log(count)

            $.ajax({
                url: 'delete/'+count,
                method: 'POST',
                data:{"_token": $('meta[name="csrf-token"]').attr('content'), "data":count},
                // dataType: 'json',
                success: function (data){
                    
                    table.destroy()
                    fetch_data()
                },
                error: function(xhr, status, error) {
                  var err = eval("(" + xhr.responseText + ")");
                  alert(err.Message);
                }
            })
        }
        
    }

    function delete_duplicate_data() {
        var count = [];
        var origin = window.location.origin;
        var table = $('table.duplicate').DataTable()

        selectedData = table.rows('.selected').data()

        for (let index = 0; index < selectedData.length; index++) {
            count.push(selectedData[index]['id'])
        }

        if(confirm("Are you sure you want to remove this?"))
        {
            // count.serialize()
            console.log(count)

            $.ajax({
                url: 'delete/'+count,
                method: 'POST',
                data:{"_token": $('meta[name="csrf-token"]').attr('content'), "data":count},
                // dataType: 'json',
                success: function (data){

                    table.destroy()
                    fetch_data()
                },
                error: function(xhr, status, error) {
                  var err = eval("(" + xhr.responseText + ")");
                  alert(err.Message);
                }
            })
        }

    }
});